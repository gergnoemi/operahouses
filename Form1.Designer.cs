﻿namespace lab2
{
    partial class frmProba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.attrText = new System.Windows.Forms.TextBox();
            this.szurGomb = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbCityFilter = new System.Windows.Forms.ComboBox();
            this.dgvOperaHouses = new System.Windows.Forms.DataGridView();
            this.operaHouseId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.operaHouseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.capacity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnExit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOperaHouses)).BeginInit();
            this.SuspendLayout();
            // 
            // attrText
            // 
            this.attrText.Location = new System.Drawing.Point(394, 63);
            this.attrText.MaxLength = 1;
            this.attrText.Name = "attrText";
            this.attrText.Size = new System.Drawing.Size(100, 22);
            this.attrText.TabIndex = 0;
            // 
            // szurGomb
            // 
            this.szurGomb.Location = new System.Drawing.Point(551, 98);
            this.szurGomb.Name = "szurGomb";
            this.szurGomb.Size = new System.Drawing.Size(75, 23);
            this.szurGomb.TabIndex = 1;
            this.szurGomb.Text = "Szűr";
            this.szurGomb.UseVisualStyleBackColor = true;
            this.szurGomb.Click += new System.EventHandler(this.szurGomb_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(217, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Operaház kezdőbetűje";
            // 
            // cbCityFilter
            // 
            this.cbCityFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCityFilter.FormattingEnabled = true;
            this.cbCityFilter.Location = new System.Drawing.Point(382, 149);
            this.cbCityFilter.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbCityFilter.Name = "cbCityFilter";
            this.cbCityFilter.Size = new System.Drawing.Size(121, 24);
            this.cbCityFilter.TabIndex = 25;
            // 
            // dgvOperaHouses
            // 
            this.dgvOperaHouses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOperaHouses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.operaHouseId,
            this.operaHouseName,
            this.address,
            this.cityId,
            this.cityName,
            this.countryName,
            this.capacity});
            this.dgvOperaHouses.Location = new System.Drawing.Point(94, 231);
            this.dgvOperaHouses.Name = "dgvOperaHouses";
            this.dgvOperaHouses.ReadOnly = true;
            this.dgvOperaHouses.RowTemplate.Height = 24;
            this.dgvOperaHouses.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOperaHouses.Size = new System.Drawing.Size(643, 150);
            this.dgvOperaHouses.TabIndex = 4;
            // 
            // operaHouseId
            // 
            this.operaHouseId.HeaderText = "Operaház ID";
            this.operaHouseId.Name = "operaHouseId";
            this.operaHouseId.ReadOnly = true;
            // 
            // operaHouseName
            // 
            this.operaHouseName.HeaderText = "Operaház neve";
            this.operaHouseName.Name = "operaHouseName";
            this.operaHouseName.ReadOnly = true;
            // 
            // address
            // 
            this.address.HeaderText = "Cím";
            this.address.Name = "address";
            this.address.ReadOnly = true;
            // 
            // cityId
            // 
            this.cityId.HeaderText = "Város ID";
            this.cityId.Name = "cityId";
            this.cityId.ReadOnly = true;
            // 
            // cityName
            // 
            this.cityName.HeaderText = "Város neve";
            this.cityName.Name = "cityName";
            this.cityName.ReadOnly = true;
            // 
            // countryName
            // 
            this.countryName.HeaderText = "Ország";
            this.countryName.Name = "countryName";
            this.countryName.ReadOnly = true;
            // 
            // capacity
            // 
            this.capacity.HeaderText = "Férőhelyek száma";
            this.capacity.Name = "capacity";
            this.capacity.ReadOnly = true;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(701, 415);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Kilép";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmProba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 476);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.dgvOperaHouses);
            this.Controls.Add(this.cbCityFilter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.szurGomb);
            this.Controls.Add(this.attrText);
            this.Name = "frmProba";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Szűrés";
            this.Load += new System.EventHandler(this.frmProba_Load);
            this.Shown += new System.EventHandler(this.frmProba_Show);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOperaHouses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox attrText;
        private System.Windows.Forms.Button szurGomb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbCityFilter;
        private System.Windows.Forms.DataGridView dgvOperaHouses;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridViewTextBoxColumn operaHouseId;
        private System.Windows.Forms.DataGridViewTextBoxColumn operaHouseName;
        private System.Windows.Forms.DataGridViewTextBoxColumn address;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityId;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn countryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn capacity;
    }
}

