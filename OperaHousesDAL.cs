﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace lab2
{
    public struct OperaHouse {
        /// <summary>
        /// Structure for storing the OperaHouse's information
        /// </summary>
        int operaHouseId;
        string operaHouseName;
        string address;
        int capacity;
        City operaHouseCity;

        public int OperaHouseId
        {
            get { return operaHouseId; }
            set { operaHouseId = value; }
        }

        public string OperaHouseName
        {
            get { return operaHouseName; }
            set { operaHouseName = value; }
        }

        public City OperaHouseCity
        {
            get { return operaHouseCity; }
            set { operaHouseCity = value; }
        }
        
        public int Capacity
        {
            get { return capacity; }
            set { capacity = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }
    }

    public class OperaHousesDAL : DALGen
    {
        public OperaHousesDAL(ref string error)
        {
            //megpróbáljuk, hogy létrejön-e a kapcsolat            
            base.CreateConnection(ref error);
        }


        /// <summary>
        /// Gets the list of all opera houses into an opera house structure list (loading the structure list with 
        /// the data from DataReader). 
        /// If a valid cityId is given, then filters the results based on the cityId and on the initial of the Opera House Name.
        /// </summary>
        /// <param name="cityId"> the ID of the opera house's city </param>
        /// <param name="initialLetter"> the initial of the opera house's name </param>
        /// <returns>the list of opera houses</returns>

        public List<OperaHouse> GetOperaHouseListDataReader(int cityId, string initialLetter, ref string error)
        {
            string query = "SELECT OperahazID, Operahazak.Nev, Cim, FeroSz, Operahazak.VarosID, Varosok.Nev as VarosNev, Orszagok.Nev as OrszagNev " +
                "FROM Operahazak, Varosok, Orszagok " +
                "WHERE Operahazak.VarosID = Varosok.VarosID AND Varosok.OrszagID = Orszagok.OrszagID";

            if (cityId >= 0)
            {
                query += " and Varosok.VarosID = " + cityId;
            }

            if (initialLetter != " ")
            {
                query += " and Operahazak.Nev LIKE '" + initialLetter + "%' ";
            }

            SqlDataReader dataReader = ExecuteReader(query, ref error);
            List<OperaHouse> operaHouseList = new List<OperaHouse>();

            if (error == "OK")
            {
                OperaHouse item = new OperaHouse();
                while (dataReader.Read())
                {
                    try
                    {
                        item.OperaHouseId = Convert.ToInt32(dataReader["OperahazID"]);
                        item.OperaHouseName = dataReader["Nev"].ToString();
                        item.Address = dataReader["Cim"].ToString();
                        item.OperaHouseCity = new City(Convert.ToInt32(dataReader["VarosID"]), dataReader["VarosNev"].ToString(), dataReader["OrszagNev"].ToString()) ;
                        item.Capacity = Convert.ToInt32(dataReader["FeroSz"]);
                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }
                    operaHouseList.Add(item);
                }
            }
            CloseDataReader(dataReader);

            return operaHouseList;
        }


        /// ugyanaz, mint az elozo, csak itt DataSet-tel dolgozunk 
        /// egyelore nincs hasznalva
        public List<OperaHouse> GetOperaHouseListDataSet(int cityId, string initialLetter, ref string error)
        {
            string query = "SELECT OperahazID, Operahazak.Nev, Cim, FeroSz, Operahazak.VarosID, Varosok.Nev as VarosNev, Orszagok.Nev as OrszagNev " +
                "FROM Operahazak, Varosok, Orszagok " +
                "WHERE Operahazak.VarosID = Varosok.VarosID AND Varosok.OrszagID = Orszagok.OrszagID";

            if (cityId >= 0)
            {
                query += " and Varosok.VarosID = " + cityId;
            }

            if (initialLetter != " ")
            {
                query += " and Operahazak.Nev LIKE '" + initialLetter + "%' ";
            }

            DataSet ds_tabla = new DataSet();
            ds_tabla = ExecuteDS(query, ref error);

            List<OperaHouse> operaHouseList = new List<OperaHouse>();

            if (error == "OK")
            {
                OperaHouse item = new OperaHouse();
                foreach (DataRow r in ds_tabla.Tables[0].Rows)
                {
                    try
                    {
                        item.OperaHouseId = Convert.ToInt32(r["OperahazID"]);
                        item.OperaHouseName = r["Nev"].ToString();
                        item.Address = r["Cim"].ToString();
                        item.OperaHouseCity = new City(Convert.ToInt32(r["VarosID"]), r["VarosNev"].ToString(), r["OrszagNev"].ToString());
                        item.Capacity = Convert.ToInt32(r["FeroSz"]);
                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }

                    operaHouseList.Add(item);
                }
            }
            return operaHouseList;
        }
}
    }
