﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2
{
    public partial class frmProba : Form
    {
        private CitiesDAL citiesDAL;
        private OperaHousesDAL operaHousesDAL;
        private string errMess;
        private int errNumber;
        private bool first;

        public frmProba()
        {
            InitializeComponent();
            string error = string.Empty;
            operaHousesDAL = new OperaHousesDAL(ref error);
            if (error != "OK")
            {
                errNumber = 1;
                errMess = "Error" + errNumber + Environment.NewLine + "OperaHouses objektumot nem tudtam letrehozni. " + error;
                Console.WriteLine(errMess);
            }
            else
            {
                errMess = "OK";
                citiesDAL = new CitiesDAL();
                Console.WriteLine("letrejott a kapcsolat");
            }
        }

        private void frmProba_Load(object sender, EventArgs e)
        {
            if (errMess == "OK") //letrejott az OperaHouses objektum
            {
                FillCbCities();

                FillDgvOperaHouses(-1, "");

                // Inicializaljuk a ListView-t  
                // Megj: DataGridView eseten konnyebb dolgunk van, 
                // feluletrol mindezeket a beallitasokat konnyen elvegezhetjuk (lasd. FillForm.cs[Design]), 
                // majd a DataSource property-jet a feltoltendo adatokat tartalmazo DataSet-re allitjuk
                //InitializeListView();
                //first = true;//ez az elso alkalom, hogy feltoltjuk a ListView-t adatokkal
                //FillLvHouses(-1);
                //first = false;
                // lblHouseNo.Text = m_Houses.GetHouseNumber().ToString();
            }
            else
            {
                Console.WriteLine(errMess);
            }
        }

        private void FillCbCities()
        {
            string error = string.Empty;
            List<City> cityList = citiesDAL.GetCityList(ref error);

            if (error != "OK")
            {
                errNumber++;
                if (errMess == "OK")
                    errMess = string.Empty;
                errMess = errMess + Environment.NewLine +
                    "Error" + errNumber + Environment.NewLine + "Hiba a ComboBox feltoltesenel." + error;
                Console.WriteLine(errMess);
            }
            else
            {
                //fill the cbCountryFilter combobox
                cbCityFilter.DataSource = cityList;
                //the text to be dispayed as the combobox text
                cbCityFilter.DisplayMember = "CityName";
                //the value of the combobox (can be accessed by the selectedValue property)
                cbCityFilter.ValueMember = "CityID";
            }

        }

        private void FillDgvOperaHouses(int cityID, string initialLetter)
        {
            string error = string.Empty;
            dgvOperaHouses.Rows.Clear();
            List<OperaHouse> operaHouseList = operaHousesDAL.GetOperaHouseListDataSet(cityID, initialLetter, ref error);

            if ((operaHouseList.Count != 0) && (error == "OK")) //ha van a felteteleknek eleget tevo nyaralo es nem lepett fel hiba,
            //a lista elemeit hozzaadjuk a DataGridView-hoz(lesznek olyan oszlopok/cellak, melyek 
            //nem jelennek meg a DataGridView-ban
            {
                foreach (OperaHouse item in operaHouseList)
                {
                    try
                    {
                        dgvOperaHouses.Rows.Add(item.OperaHouseId,
                                           item.OperaHouseName,
                                           item.Address,
                                           item.OperaHouseCity.CityId,
                                           item.OperaHouseCity.CityName,
                                           item.OperaHouseCity.Country,
                                           item.Capacity);
                    }
                    catch (Exception ex)
                    {
                        errNumber++;
                        if (errMess == "OK") errMess = string.Empty;
                        errMess = errMess + Environment.NewLine +
                            "Error" + errNumber + Environment.NewLine + "Invalid data " + ex.Message;
                    }
                }
            }
            else if (error != "OK")
            {
                errNumber++;
                if (errMess == "OK") errMess = string.Empty;
                errMess = errMess + Environment.NewLine +
                    "Error" + errNumber + Environment.NewLine + "Hiba a DataGridView feltoltesenel." + error;
                Console.WriteLine(errMess);
            }
        }

        private void szurGomb_Click(object sender, EventArgs e)
        {
            //cbCountryFilter.SelectedValue -> countryID
            //cbCountryFilter.SelectedText -> countryName
            FillDgvOperaHouses(Convert.ToInt32(cbCityFilter.SelectedValue), Convert.ToString(attrText.Text));
            //FillLvHouses(Convert.ToInt32(cbCountryFilter.SelectedValue));

            if (errMess != "OK")
            {
                Console.WriteLine(errMess);
                //ErrorForm errorForm = new ErrorForm(errMess);
                //errorForm.Show();
                //errorForm.Focus();
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmProba_Show(object sender, EventArgs e)
        {
            if (errMess != "OK")
            {
                Console.WriteLine(errMess);
                //ErrorForm errorForm = new ErrorForm(errMess);
                //errorForm.Show();
                //errorForm.Focus();
            }
        }
    }
}
