﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;

namespace lab2
{
    /// <summary>
    /// Data structure for storing city's Information
    /// </summary>
    public struct City
    {
        int cityId;
        string cityName;
        string country;

        public int CityId
        {
            get { return cityId; }
            set { cityId = value; }
        }

        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        public City(int cID, string cName, string cCountry)
        {
            cityId = cID;
            cityName = cName;
            country = cCountry;
        }
    }

    public class CitiesDAL : DALGen
    {
        /// <summary>
        /// Gets the list of all cities
        /// </summary>
        /// <returns></returns>
        /// 
        public List<City> GetCityList(ref string error)
        {
            string query = "SELECT VarosID, Varosok.Nev, Varosok.OrszagID, Orszagok.Nev FROM Varosok, Orszagok WHERE Varosok.OrszagID = Orszagok.OrszagID";
            SqlDataReader dataReader = ExecuteReader(query, ref error);

            List<City> cityList = new List<City>();

            if (error == "OK")
            {
                City item = new City();
                while (dataReader.Read())
                {
                    try
                    {
                        item.CityId = Convert.ToInt32(dataReader[0]);
                        item.CityName = dataReader[1].ToString();
                        item.Country = dataReader[3].ToString();
                        cityList.Add(item);
                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }
                }
            }
            CloseDataReader(dataReader);

            return cityList;
        }
    }
}
